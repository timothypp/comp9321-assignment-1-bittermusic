<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="assign1.*, java.util.*"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<%@ include file="style.html" %>
	
	<title>Welcome to BitterMusic</title>
</head>
<body background="default_back.jpg">

<%
	MusicDB musicDatabase = new MusicDB();
	Cart theCart = new Cart();
	
	ArrayList<Song> randomSong = new ArrayList<Song>();
	
	try{		
	  	musicDatabase = (MusicDB)session.getAttribute("MusicDB");
	  	theCart = (Cart)session.getAttribute("UserCart");

		List<?> result = (List<?>)request.getAttribute("RandomSong");
	  	
	  	if (musicDatabase == null || theCart == null || result == null){
	  		response.sendRedirect("./search");
	  		return;
  		}
	  	
		for (Object object : result) {
		    if (object instanceof Song) {
		    	randomSong.add((Song) object);
		    }
		}
	}
	catch(Exception e){
  		// Go to Welcome page when there is error. (Usually caused by session not defined)
		response.sendRedirect("./search");
  		return;
	}
%>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="./search">
	            	<span class="glyphicon glyphicon-music"></span> 
	            	BitterMusic
	            </a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav">
	                <li class="active">
	                    <a href="./search">Search</a>
	                </li>
     		        <li class="">
	                    <a href="./search?act=about">About</a>
	                </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
					<li class="navbar-right">
	            		<a href="./search?act=cart">Shopping Cart (<%= theCart.getTotalItemInCart() %>)</a>
	            	</li>
	            </ul>
	        </div>
	    </div>
	</nav>
	
	<div class="container">
		<center>
	        <h1 class="page-header"></h1>
		    <div class="row">
		        <div class="col-xs-12 col-md-12">
		        	<h2 style="color:White">
		        		Welcome to BitterMusic.
		        	</h2>
			    	<h3 style="color:White">
			    		Music for Everyone. Find your favourite Music.
			    	</h3>
			    	<h3 class="page-header"></h3>
			    	<h3 style="color:White">
			    		Search
					</h3>
					<h4 style="color:White">
						Know what you want to listen to? Just search and hit play.
					</h4>
			    </div>
			    <div class="col-xs-12 col-md-12">
			    	<br>
			    </div>
				<div class="col-xs-10 col-xs-offset-1 col-md-offset-1 col-md-10 img-rounded" style="background-color:White">
					<p><h5 style="color:Red" name="error_message_search" id="error_message_search"></h5></p>
					<p>
						<form action="./search" method="GET" onSubmit="return validation_search(this)">
							<div class="form-group" id="div-search" name="div-search">
								<input type="text" class="form-control" id="search" name="search" placeholder="Search for Artists, Titles, Albums or Songs"></input>
							</div>
							<input type="checkbox" name="advanced" id="advanced">    Search Options    </input>
							<br>
							<br>
							<div class="form-group" id="div-advance" name="div-advance" style="display:none">
								<table>
									<tr>
										<td>
											Search for:
										</td>
										<td>
											<select name='search_option'>
												<option value="anything" selected>Anything</option>
												<option value="album">Album</option>
												<option value="artist">Artist</option>
												<option value="songs">Songs</option>
											</select>													
										</td>
									</tr>
								</table>
							</div>
							<br/>
							<button type="submit" class="btn btn-default" name="act" id="act" value="search">Search</button>
						</form>
					</p>
				</div>
				<div class="col-xs-12 col-md-12">
			    	<br>
			    </div>
			    <div class="col-xs-12 col-md-12">
			    	<h3 class="page-header"></h3>
			    	<h3 style="color:White">
			    		BitterMusic Random Pick
					</h3>
					<h4 style="color:White">
						Discover new Songs in BitterMusic and Randomize Your World.
					</h4>
			    </div>
			    <div class="col-xs-12 col-md-12">
			    	<br>
			    </div>
				<div class="col-xs-10 col-xs-offset-1 col-md-offset-1 col-md-10 img-rounded" style="background-color:White">					
					<div class="row" align="left">
							<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
					       		<p>
					       			<% for (int i = 0; i < randomSong.size(); i++){ 
       									Song random = randomSong.get(i);
					       			%>
					       			
									<h4 style="color:Silver">
										<%= i + 1 %>.
										<img class="img-rounded" height="32" width="32" src="song.png" alt=""> 
										<a style="color:#0099FF" href="<%= "./search?id=" + random.getSongID() + "&type=song&act=details" %>"><%= random.getTitle() %></a> 									
										by <a href="<%= "./search?search=" + random.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (random.getArtist() != null) ? random.getArtist() : "N/A" %></a>
									</h4>
									<p style="padding:0px" class="modal-header"></p>
									
									<%  } %>
								</p>
							</div>
					</div>
				</div>
			</div>
		</center>
	</div>
	
	<%@ include file="footer.html" %>
	
	<script type="text/javascript">
		function validation_search(form){									
			var search = form.search.value.trim();
			search = search.replace(" ", "");

			if(search.length == 0){
				document.getElementById("error_message_search").innerHTML = "Please enter a Search Value !";
				document.getElementById("div-search").className = "form-group has-error";
				document.getElementById("div-search").className = "form-group";

				form.search.focus();
				return false;
			}
			
			return true;
		};	
		
		$(document).ready(function(){
		    $("#advanced").click(function(){
				if (document.getElementById('advanced').checked) {
		            document.getElementById('div-advance').style.display = 'inline';
		        }
		        else{
		        	document.getElementById('div-advance').style.display = 'none';
		        }
		    });

		});
	</script>
	
</body>
</html>