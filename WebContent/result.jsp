<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="assign1.*, java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<%@ include file="style.html" %>
	
	<%
		String search = ((request.getParameter("search") == null) ? "" : request.getParameter("search").toLowerCase());
	
		if (search == null){
			response.sendRedirect("./search");
	  		return;
		}
	%>
	
	<title>Result for : <%= search %>  </title>
</head>
<body background="default_back.jpg">
	
<%
	MusicDB musicDatabase = new MusicDB();
	Cart theCart = new Cart();
	
	ArrayList<Album> albumResult = new ArrayList<Album>();
	ArrayList<Song> songsResult = new ArrayList<Song>();
	
	String message_error = (String)((session.getAttribute("ErrorMessage") == null) ? "" : session.getAttribute("ErrorMessage"));
	session.removeAttribute("ErrorMessage"); //Remove ErrorMessage Session Attribute
	
	try{		
	  	musicDatabase = (MusicDB)session.getAttribute("MusicDB");
	  	theCart = (Cart)session.getAttribute("UserCart");
	
		List<?> result_album = (List<?>)request.getAttribute("SearchResultAlbum");
		List<?> result_song = (List<?>)request.getAttribute("SearchResultSong");

	  	if (musicDatabase == null || theCart == null || result_album == null || result_song == null){
	  		response.sendRedirect("./search");
	  		return;
		}
  	
		for (Object object : result_album) {
		    if (object instanceof Album) {
		    	albumResult.add((Album) object);
		    }
		}
		
		for (Object object : result_song) {
		    if (object instanceof Song) {
		    	songsResult.add((Song) object);
		    }
		}
		
	}
	catch(Exception e){
		// Go to Welcome page when there is error. (Usually caused by session not defined)
		response.sendRedirect("./search");
  		return;
	}
%>
	
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="./search">
	            	<span class="glyphicon glyphicon-music"></span> 
	            	BitterMusic
	            </a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav">
	                <li class="">
	                    <a href="./search">Search</a>
	                </li>
     		        <li class="">
	                    <a href="./search?act=about">About</a>
	                </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
					<li class="navbar-right">
	            		<a href="./search?act=cart">Shopping Cart (<%= theCart.getTotalItemInCart() %>)</a>
	            	</li>
	            </ul>
	        </div>
	    </div>
	</nav>
	
	<div class="container">
		<center>		
			<form action="./search" method="POST" onSubmit="return checkCartContent(this)">
		        <h1 class="page-header"></h1>
			    <div class="row">
			    	<div class="col-xs-12 col-md-12">
				    	<h3 style="color:White">
				    		Search Result for : <%= search %>
						</h3>
				    </div>
				    
				    <!-- Displayable if there is error message -->
				    <%	if (message_error.length() > 0){	%>
				    	<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="left">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<h5 style="color:Red">
											<%= message_error %>
										</h5>
									</p>
								</div>
							</div>
						</div>
					<%	}	%>
					
					<div class="col-xs-12 col-md-12">
						<br>
					</div>
			    
				    <%
				    	long total_result = songsResult.size() + albumResult.size();
				    	if (total_result > 0){
				    %>
				    
					   	<% 	if (songsResult.size() > 0){	%>
					   	
					    <div class="col-xs-12 col-md-12">
					    	<h3 style="color:White">
					    		Songs <a style="font-size:75%; color:#0099FF" class="expandCollapseSong" href="#">(Collapse)</a>
							</h3>
					    </div>
						<div id="songSection" name="songSection" class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="left">
								<div class="panel panel-default col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table class="table" style="color:DarkGray">
						       				<thead>
						       					<th>
						       						 <input type="checkbox" class="check" id="checkAll_Song">
						       					</th>
						       					<th>
						       						#
						       					</th>
							       				<th>
													Title
							       				</th>
							       				<th>
													Artist
							       				</th>
							       				<th>
													Album
							       				</th>
							       				<th>
													Genre
							       				</th>
							       				<th>
													Publisher
							       				</th>
							       				<th>
													Year
							       				</th>
							       				<th>
													Price
							       				</th>
						       				</thead>
						       				<tbody>
							       				<% 	for (int i = 0; i <  songsResult.size();i++){	
							       						Song s = songsResult.get(i);
							       				%>
							       				<tr>
							       					<th scope="row"><input type="checkbox" class="check_song" id="sSet" name="sSet" onchange = "changeSong(this)" value="<%= s.getSongID() %>"/></th>													
							       					<th scope="row"><%= i + 1 %></th>
													<td>
														<a style="color:#0099FF" href="<%= "./search?id=" + s.getSongID() + "&type=song&act=details" %>"><%= (s.getTitle() != null) ? s.getTitle() : "N/A" %></a>												
													</td>
													<td>
														<a href="<%= "./search?search=" + s.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (s.getArtist() != null) ? s.getArtist() : "N/A" %></a>												
													</td>
													<%
													   String albumID = (s.getAlbumID() != null) ? s.getAlbumID() : "N/A"; 
													   Album a;
													   
													   String album = "N/A";
													   String genre = "N/A";
													   String publisher = "N/A";
													   String year = "N/A";
													   
													   if (!albumID.equalsIgnoreCase("n/a")){
														   a = musicDatabase.getAlbum(albumID);
														   album = (a != null) ? a.getTitle() : "N/A";
														   genre = (a != null) ? a.getGenre() : "N/A";
														   publisher = (a != null) ? a.getPublisher() : "N/A";
														   year = (a != null) ? a.getYear(): "N/A";
													   }
													%>
													
													<td>
													   <a style="color:#0099FF" href="<%= "./search?id=" + albumID + "&type=album&act=details" %>"><%= album %></a>																									
													</td>
													<td>
														<%= genre %>												
													</td>
													<td>
														<%= publisher %>												
													</td>
													<td>
														<%= year %>												
													</td>
													<td>
														$<%= (s.getPrice() != null) ? String.format("%.2f", Double.parseDouble(s.getPrice())) : "N/A" %>											
													</td>
												</tr>
												<% 	}	%>
											</tbody>
						       			</table>
									</p>
								</div>
							</div>
						</div>
						
						<%	}	%>				
						<%	if (albumResult.size() > 0){	%>
						
						<div class="col-xs-12 col-md-12">
					    	<h3 style="color:White">
					    		Albums <a style="font-size:75%; color:#0099FF" class="expandCollapseAlbum" href="#">(Collapse)</a>
							</h3>
					    </div>
						<div id="albumSection" name="albumSection" class="col-xs-12 col-md-12 img-rounded" style="background-color:White">									
							<div class="row" align="left">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table class="table" style="color:DarkGray">
						       				<thead>
						       					<th>
						       						 <input type="checkbox" class="check" id="checkAll_Album">
						       					</th>
						       					<th>
						       						#
						       					</th>
							       				<th>
													Title
							       				</th>
							       				<th>
													Artist
							       				</th>
							       				<th>
													Genre
							       				</th>
							       				<th>
													Publisher
							       				</th>
							       				<th>
													Year
							       				</th>
							       				<th>
													Price
							       				</th>
						       				</thead>
						       				<tbody>
							       				<% 	for (int i = 0; i <  albumResult.size();i++){	
							       						Album a = albumResult.get(i);
							       				%>
							       				<tr>
							       					<th scope="row"><input type="checkbox" class="check_album" id="aSet" name="aSet" onchange = "changeAlbum(this)" value="<%= a.getID() %>"/></th>
							       					<th scope="row"><%= i + 1 %></th>
													<td>
													   <a style="color:#0099FF" href="<%= "./search?id=" + a.getID() + "&type=album&act=details" %>"><%= (a.getTitle() != null) ? a.getTitle() : "N/A" %></a>																									
													</td>
													<td>
														<a href="<%= "./search?search=" + a.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (a.getArtist() != null) ? a.getArtist() : "N/A" %></a>												
													</td>													
													<td>
														<%= (a.getGenre() != null) ? a.getGenre() : "N/A" %>														
													</td>
													<td>
														<%= (a.getPublisher() != null) ? a.getPublisher() : "N/A" %>														
													</td>
													<td>
														<%= (a.getYear() != null) ? a.getYear() : "N/A" %>														
													</td>
													<td>
														$<%= (a.getPrice() != null) ? String.format("%.2f", Double.parseDouble(a.getPrice())) : "N/A" %>												
													</td>
												</tr>
												<% 	}	%>
											</tbody>
						       			</table>
									</p>
								</div>
							</div>
						</div>
					    
					    <%	}	%>
					    
					    <div class="col-xs-12 col-md-12">
					    	<br>
					    </div>
					    <div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="center">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table>
						       				<tr>
						       					<td>
						       						<button id="btnBackSearch" name="btnBackSearch" type="submit" class="btn btn-default"  onclick='return btnBackToSearch();'>Back to Search</button>
						       					</td>
						       					<td>
						       						<button type="submit" class="btn btn-default" name="act" id="act" value="add_to_cart">Add to Cart</button>
						       					</td>
						       				</tr>
						       			</table>
									</p>
								</div>
							</div>
						</div>
					    
				    <%	}
				    	else{
				    %>
				    
				    	<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="left">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<h4 style="color:Red; text-align:center">
						       				Sorry, no matching datasets found! Try different search string!
						       			</h4>
									</p>
								</div>
							</div>
						</div>
					    <div class="col-xs-12 col-md-12">
					    	<br>
					    </div>
						<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="center">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table>
						       				<tr>
						       					<td>
						       						<button id="btnBackSearch" name="btnBackSearch" type="submit" class="btn btn-default"  onclick='return btnBackToSearch();'>Back to Search</button>
						       					</td>
						       				</tr>
						       			</table>
									</p>
								</div>
							</div>
						</div>
				    
				    <%	}	%>
				</div>				
			</form>
		</center>
	</div>
	
	<%@ include file="footer.html" %>
	
	<script type="text/javascript">

		function checkCartContent(form){
			var count_check = 0;
			
			$('input:checked[name=sSet]').each(function(){
				count_check++;
			});
			
			$('input:checked[name=aSet]').each(function(){
				count_check++;
			});
			
			if (count_check > 0)
				return true;
			else
				return false;
			
	    	//var message_error = "";
	    	//var count_error = 0;    	
			
			//var cart_song = {};
			//var cart_album = {};
			//var cart_album_song = {};
			
			//var selected_song = [];
			//var selected_album = [];
			
			<%-- for (Song s : theCart.getSongCart()){ %>
				cart_song["<%= s.getSongID() %>"] = "<%= s.getTitle() %>";
			<% } %>
			
			<% for (Album a : theCart.getAlbumCart()){ %>
				cart_album["<%= a.getID() %>"] = "<%= a.getTitle() %>";
				
				<% for (Song s : musicDatabase.getSongList()){ 
					if (s.getAlbumID().equalsIgnoreCase(a.getID())){ %>
						cart_album_song["<%= s.getSongID() %>"] = "<%= s.getTitle() %>";
					<% } %>
				<% } %>
			<% } --%>			
			
			//$('input:checked[name=sSet]').each(function(){
				//var song_id = $(this).val();
		    	
		    	//if (song_id in cart_song){
		    		//count_error++;
		    		//message_error += "The Song : \"" + cart_song[song_id] + "\" is in cart!\n";
		    	//}
		    	//else if (song_id in cart_album_song){
		    		//count_error++;
		    		//message_error += "The Song : \"" + cart_album_song[song_id] + "\" is part of an Album in cart!\n";
		    	//}
		    	
		    	//selected_song.push(song_id);
		    //});
			
			//$('input:checked[name=aSet]').each(function(){
		    	//var album_id = $(this).val();
		    	
		    	//if (album_id in cart_album){
		    		//count_error++;
		    		//message_error += "The Album : \"" + cart_album[album_id] + "\" is in cart!\n";
		    	//}
		    	
		    	//selected_album.push(album_id);	    	
		    //});
		    
		    //if ((selected_song.length + selected_album.length) == 0){
		    	//message_error = 'Please choose the Album(s) or Song(s) that you want to Purchase!\n';
		    	//count_error++;
		    //}
		    
		    //if (count_error == 0){
		    	//return true;
		    //}
		    //else{
		    	//alert(message_error);
				//return false;	
		    //}	
		    
		    //return true;
		}
	
		function btnBackToSearch() {
		    window.location.href = "./search";
			return false;
		}
		
		function changeSong(checkboxElem){
    		var total_result = <%= songsResult.size() %>;
    		var count = 0;
    		
    		$('input:checked[name=sSet]').each(function(){
    			++count;
    		});
    		    		
    		if (total_result == count){
    			$("#checkAll_Song").prop('checked', 1);
    		}
    		else{
    			$("#checkAll_Song").prop('checked', 0);
    		}
		}
		
		function changeAlbum(checkboxElem){
    		var total_result = <%= albumResult.size() %>;
    		var count = 0;
    		
    		$('input:checked[name=aSet]').each(function(){
    			++count;
    		});
    		    		
    		if (total_result == count){
    			$("#checkAll_Album").prop('checked', 1);
    		}
    		else{
    			$("#checkAll_Album").prop('checked', 0);
    		}
		}
	
		$(document).ready(function () {
	        $(".expandCollapseSong").click(function () {
	        	var value = $(this).text().toLowerCase();
	        	
	        	if (value == "(collapse)"){
	        		$('.expandCollapseSong')[0].innerHTML = "(Expand)";
	            	$("#songSection").hide("slow");
	        	}
	        	else{
	        		$('.expandCollapseSong')[0].innerHTML = "(Collapse)";
	            	$("#songSection").show("slow");
	        	}
	        });
	        
	        $(".expandCollapseAlbum").click(function () {
	        	var value = $(this).text().toLowerCase();
	        	
	        	if (value == "(collapse)"){
	        		$('.expandCollapseAlbum')[0].innerHTML = "(Expand)";
	            	$("#albumSection").hide("slow");
	        	}
	        	else{
	        		$('.expandCollapseAlbum')[0].innerHTML = "(Collapse)";
	            	$("#albumSection").show("slow");
	        	}
	        });

	        $("#checkAll_Song").click(function () {
	            $(".check_song").prop('checked', $(this).prop('checked'));
	        });
	        
	        $("#checkAll_Album").click(function () {
	            $(".check_album").prop('checked', $(this).prop('checked'));
	        });
	        
	        $("#sSet").click(function () {
        		var total_result = <%= songsResult.size() %>;
        		var count = 0;
        		
        		$('input:checked[name=sSet]').each(function(){
        			count++;
        		});
        		
        		if (total_result == count){
        			$("#checkAll_Song").prop('checked', 1);
        		}   	
	        });
	        
	        $("#aSet").click(function () {
        		var total_result = <%= albumResult.size() %>;
        		var count = 0;
        		
        		$('input:checked[name=aSet]').each(function(){
        			count++;
        		});
        		
        		if (total_result == count){
        			$("#checkAll_Album").prop('checked', 1);
        		}    	
	        });
	    });
	</script>
	
</body>
</html>