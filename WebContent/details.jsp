<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="assign1.*, java.util.*"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <%@ include file="style.html" %>
    
	<title>Details Here</title>
</head>
<body background="default_back.jpg">

<%
	MusicDB musicDatabase = new MusicDB();
	Cart theCart = new Cart();
	
	String type = "";
	Album a = new Album();
	Song s = new Song();

	String message_error = (String)((session.getAttribute("ErrorMessage") == null) ? "" : session.getAttribute("ErrorMessage"));
	session.removeAttribute("ErrorMessage"); //Remove ErrorMessage Session
	
	try{	
	  	musicDatabase = (MusicDB)session.getAttribute("MusicDB");
	  	theCart = (Cart)session.getAttribute("UserCart");
	  	
	  	type = (String)request.getAttribute("Type");
	  	
	  	if (musicDatabase == null || theCart == null || type == null){
	  		response.sendRedirect("./search");
	  		return;
  		}
	  	
	  	if (type.equalsIgnoreCase("album")){
	  		a = (Album)request.getAttribute("Data");
	  		
	  		if (a == null){
	  			response.sendRedirect("./search");
		  		return;
	  		}
	  	}
	  	else if (type.equalsIgnoreCase("song")){
	  		s = (Song)request.getAttribute("Data");
	  		
	  		if (s == null){
	  			response.sendRedirect("./search");
		  		return;
	  		}
	  	}
	  	else{
	  		response.sendRedirect("./search");
	  		return;
	  	}
	}
	catch(Exception e){
  		// Go to Welcome page when there is error. (Usually caused by session not defined)
		response.sendRedirect("./search");
  		return;
	}
	
%>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="./search">
	            	<span class="glyphicon glyphicon-music"></span> 
	            	BitterMusic
	            </a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav">
	                <li class="">
	                    <a href="./search">Search</a>
	                </li>
     		        <li class="">
	                    <a href="./search?act=about">About</a>
	                </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
					<li class="navbar-right">
	            		<a href="./search?act=cart">Shopping Cart (<%= theCart.getTotalItemInCart() %>)</a>
	            	</li>
	            </ul>
	        </div>
	    </div>
	</nav>
	
	<div class="container">
		<center>		
	        <div class="row">
			    	<div class="col-lg-12">
			    	
			    	<!-- Displayable if there is error message -->
				    <%	if (message_error.length() > 0){	%>
				    	<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="left">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<h5 style="color:Red">
											<%= message_error %>
										</h5>
									</p>
								</div>
							</div>
						</div>
					<%	}	%>
					
					<div class="col-xs-12 col-md-12">
						<br>
					</div>
			    	
					<form action="./search" method="POST" onSubmit="return checkCartContent(this)">
						<h2 style="color:White" class="page-header">
			        		<%= type.substring(0, 1).toUpperCase() + type.substring(1) %> Details
			        	</h2>
			        	
				    	<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="center">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<h4>
						       				<img src="song.png" alt="SongPic" height="20%" width="20%">
						       			</h4>
						       			
						       			<table class="table" style="color:DarkGray">						       			
						       				<tbody>						       				
						       					<%	if (type.equalsIgnoreCase("song")){	%>
						       						<%
													   String albumID = (s.getAlbumID() != null) ? s.getAlbumID() : "N/A"; 
													   
													   String album = "N/A";
													   String genre = "N/A";
													   String publisher = "N/A";
													   String year = "N/A";
													   
													   if (!albumID.equalsIgnoreCase("n/a")){
														   a = musicDatabase.getAlbum(albumID);
														   album = (a != null) ? a.getTitle() : "N/A";
														   genre = (a != null) ? a.getGenre() : "N/A";
														   publisher = (a != null) ? a.getPublisher() : "N/A";
														   year = (a != null) ? a.getYear(): "N/A";
													   }
													%>		       					
							       					<tr>
							       						<td>
							       							<input type="hidden" id="sSet" name="sSet" value="<%= s.getSongID() %>"/>
							       							Title
							       						</td>
							       						<td>
							       							<%= (s.getTitle() != null) ? s.getTitle() : "N/A" %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Artist
							       						</td>
							       						<td>
															<a href="<%= "./search?search=" + s.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (s.getArtist() != null) ? s.getArtist() : "N/A" %></a>								
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Album
							       						</td>
							       						<td>
															<a style="color:#0099FF" href="<%= "./search?id=" + albumID + "&type=album&act=details" %>"><%= album %></a>																									
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Genre
							       						</td>
							       						<td>
							       							<%= genre %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Publisher
							       						</td>
							       						<td>
							       							<%= publisher %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Year
							       						</td>
							       						<td>
							       							<%= year %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Price
							       						</td>
							       						<td>
															$<%= (s.getPrice() != null) ? String.format("%.2f", Double.parseDouble(s.getPrice())) : "N/A" %>
							       						</td>
							       					</tr>
						       					<%	}
						       						else if (type.equalsIgnoreCase("album")){ %>
						       						<tr>
							       						<td>
							       							<input type="hidden" id="aSet" name="aSet" value="<%= a.getID() %>"/>
							       							Title
							       						</td>
							       						<td>
							       							<%= (a.getTitle() != null) ? a.getTitle() : "N/A" %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Artist
							       						</td>
							       						<td>
															<a href="<%= "./search?search=" + a.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (a.getArtist() != null) ? a.getArtist() : "N/A" %></a>								
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Genre
							       						</td>
							       						<td>
							       							<%= (a.getGenre() != null) ? a.getGenre() : "N/A" %>	
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Publisher
							       						</td>
							       						<td>
							       							<%= (a.getPublisher() != null) ? a.getPublisher() : "N/A" %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Year
							       						</td>
							       						<td>
							       							<%= (a.getYear() != null) ? a.getYear() : "N/A" %>
							       						</td>
							       					</tr>
							       					<tr>
							       						<td>
							       							Price
							       						</td>
							       						<td>
							       							$<%= (a.getPrice() != null) ? String.format("%.2f", Double.parseDouble(a.getPrice())) : "N/A" %>
							       						</td>
							       					</tr>
						       					<%	}	%>
						       				</tbody>
						       			</table>
						       			
						       			<%	if (type.equalsIgnoreCase("album")){	%>
							       			<h4 class="page-header" style="color:#0099FF">
							       				Song Lists
							       			</h4>
							       			<table class="table" style="color:DarkGray">
							       				<thead>
							       					<th>
							       						#
							       					</th>
								       				<th>
														Title
								       				</th>
								       				<th>
														Artist
								       				</th>
								       				<th>
														Album
								       				</th>
								       				<th>
														Genre
								       				</th>
								       				<th>
														Publisher
								       				</th>
								       				<th>
														Year
								       				</th>
								       				<th>
														Price
								       				</th>
							       				</thead>
							       				<tbody>
							       					<% long count = 0;
							       						for (int i = 0; i <  musicDatabase.getSongList().size();i++){	
							       						Song song = musicDatabase.getSongList().get(i);
							     
							       						if (song.getAlbumID().equalsIgnoreCase(a.getID())){
							       					%>
							       						<tr>
							       							<th scope="row"><%= ++count %></th>
															<td>
																<a style="color:#0099FF" href="<%= "./search?id=" + song.getSongID() + "&type=song&act=details" %>"><%= (song.getTitle() != null) ? song.getTitle() : "N/A" %></a>												
															</td>
															<td>
																<a href="<%= "./search?search=" + song.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (song.getArtist() != null) ? song.getArtist() : "N/A" %></a>								
															</td>
															<%
															   String albumID = a.getID();
															   
															   String album = "N/A";
															   String genre = "N/A";
															   String publisher = "N/A";
															   String year = "N/A";
															   
															   if (!albumID.equalsIgnoreCase("n/a")){
																   a = musicDatabase.getAlbum(albumID);
																   album = (a != null) ? a.getTitle() : "N/A";
																   genre = (a != null) ? a.getGenre() : "N/A";
																   publisher = (a != null) ? a.getPublisher() : "N/A";
																   year = (a != null) ? a.getYear(): "N/A";
															   }
															%>
															
															<td>
																<a style="color:#0099FF" href="<%= "./search?id=" + albumID + "&type=album&act=details" %>"><%= album %></a>																									
															</td>
															<td>
																<%= genre %>												
															</td>
															<td>
																<%= publisher %>												
															</td>
															<td>
																<%= year %>												
															</td>
															<td>
																$<%= (song.getPrice() != null) ? String.format("%.2f", Double.parseDouble(song.getPrice())) : "N/A" %>
															</td>
														</tr>
							       					<%		}	
							       						}
							       					%>
							       				</tbody>
							       			</table>
						       			<%	}	%>
						       			
									</p>
								</div>
							</div>
						</div>
					
						<div class="col-xs-12 col-md-12">
					    	<br>
					    </div>

						<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="center">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table>
						       				<tr>
						       					<td>
						       						<button id="btnBackSearch" name="btnBackSearch" type="submit" class="btn btn-default"  onclick='return btnBackToSearch();'>Back to Search</button>
						       					</td>
						       					<td>
						       						<button type="submit" class="btn btn-default" name="act" id="act" value="add_to_cart_detail">Add to Cart</button>
						       					</td>
						       				</tr>
						       			</table>
									</p>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</center>
	</div>

	<%@ include file="footer.html" %>

	<script type="text/javascript">

		function checkCartContent(form){
			
	    	var message_error = "";
	    	var count_error = 0;    	
			
			var cart_song = {};
			var cart_album = {};
			var cart_album_song = {};
			
			var selected_song = [];
			var selected_album = [];
			
			<% for (Song song : theCart.getSongCart()){ %>
				cart_song["<%= song.getSongID() %>"] = "<%= song.getTitle() %>";
			<% } %>
			
			<% for (Album album : theCart.getAlbumCart()){ %>
				cart_album["<%= album.getID() %>"] = "<%= album.getTitle() %>";
				
				<% for (Song song : musicDatabase.getSongList()){ 
					if (song.getAlbumID().equalsIgnoreCase(album.getID())){ %>
						cart_album_song["<%= song.getSongID() %>"] = "<%= song.getTitle() %>";
					<% } %>
				<% } %>
			<% } %>			
			
			<% if (type.equalsIgnoreCase("song")){			
			%>
				var song_id = <%= s.getSongID() %>
				
				if (song_id in cart_song){
		    		count_error++;
		    		message_error += "The Song : \"" + cart_song[song_id] + "\" is in cart!\n";
		    	}
		    	else if (song_id in cart_album_song){
		    		count_error++;
		    		message_error += "The Song : \"" + cart_album_song[song_id] + "\" is part of an Album in cart!\n";
		    	}
				
			<%	}	
				else if (type.equalsIgnoreCase("album")){	%>
		    		var album_id = <%= a.getID() %>
					
		    		if (album_id in cart_album){
			    		count_error++;
			    		message_error += "The Album : \"" + cart_album[album_id] + "\" is in cart!\n";
			    	}
			<%	} %>

		    if (count_error == 0){
		    	return true;
		    }
		    else{
		    	alert(message_error);
				return false;	
		    }		    
		}
	
		function btnBackToSearch() {
		    window.location.href = "./search";
			return false;
		}
		
	</script>

</body>
</html>