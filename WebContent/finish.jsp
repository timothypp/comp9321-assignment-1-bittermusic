<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="assign1.*, java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<%@ include file="style.html" %>
	
	<%
		String message = (String)((request.getAttribute("DisplayMessage") == null) ? "" : request.getAttribute("DisplayMessage"));
	%>
	 
	<title><%= message %></title>
</head>
<body background="default_back.jpg">
	
<%
	MusicDB musicDatabase = new MusicDB();
	Cart theCart = new Cart();
	
	try{		
	  	musicDatabase = (MusicDB)session.getAttribute("MusicDB");
	  	theCart = (Cart)session.getAttribute("UserCart");
	
	  	if (musicDatabase == null || theCart == null || message.length() == 0){
	  		response.sendRedirect("./search");
	  		return;
		}
	  	
	  	session.removeAttribute("UserCart"); //Remove UserCart Session Attribute
		session.removeAttribute("MusicDB"); //Remove MusicDB Session Attribute
		
		session.invalidate(); //Invalidate the Session
	}
	catch(Exception e){
		// Go to Welcome page when there is error. (Usually caused by session not defined)
		response.sendRedirect("./search");
  		return;
	}
%>	

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="./search">
	            	<span class="glyphicon glyphicon-music"></span> 
	            	BitterMusic
	            </a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav">
	                <li class="">
	                    <a href="./search">Search</a>
	                </li>
     		        <li class="">
	                    <a href="./search?act=about">About</a>
	                </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
					<li class="navbar-right">
	            		<a href="./search?act=cart">Shopping Cart (<%= theCart.getTotalItemInCart() %>)</a>
	            	</li>
	            </ul>
	        </div>
	    </div>
	</nav>
	
	<div class="container">
		<center>		
	        <div class="row">
			    	<div class="col-lg-12">
		
					<h2 style="color:White" class="page-header">
		        		Thank You from BitterMusic!
		        	</h2>
		        	
			    	<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
						<div class="row" align="center">
							<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
					       		<p>
					       			<h4 style="color:#0099FF">
										<%= message %>
									</h4>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</center>
	</div>
	
	<%@ include file="footer.html" %>

</body>
</html>