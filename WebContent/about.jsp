<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="assign1.*, java.util.*"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<%@ include file="style.html" %>
	
	<title>About BitterMusic</title>
</head>
<body background="default_back.jpg">

<%
	MusicDB musicDatabase = new MusicDB();
	Cart theCart = new Cart();

	try{	
	  	musicDatabase = (MusicDB)session.getAttribute("MusicDB");
	  	theCart = (Cart)session.getAttribute("UserCart");
	  	
	  	if (musicDatabase == null || theCart == null){
	  		response.sendRedirect("./search");
	  		return;
  		}
	}
	catch(Exception e){
  		// Go to Welcome page when there is error. (Usually caused by session not defined)
  		response.sendRedirect("./search");
  		return;
	}
%>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="./search">
	            	<span class="glyphicon glyphicon-music"></span> 
	            	BitterMusic
	            </a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav">
	                <li class="">
	                    <a href="./search">Search</a>
	                </li>
     		        <li class="active">
	                    <a href="./search?act=about">About</a>
	                </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
					<li class="navbar-right">
	            		<a href="./search?act=cart">Shopping Cart (<%= theCart.getTotalItemInCart() %>)</a>
	            	</li>
	            </ul>
	        </div>
	    </div>
	</nav>

	<div class="container">
		<center>
		    <div class="row">
		        <div class="col-lg-12">
			
					<h2 style="color:White" class="page-header">
		        		BitterMusic Story
		        	</h2>
			
					<div class="col-xs-12 col-lg-12 img-rounded" style="background-color:White">
						<p>
							<h3 style="color:#0099FF">
								What is BitterMusic? Let me tell you a story...
							</h3>
						</p>
						<p>
							<h4 style="color:#0099FF">
								<u>History</u>
							</h4>
						</p>
						<p>
							<b>Andrew</b> has noticed students are often complaining about finding and buying Music on Online Store.
						</p>
						<p>
							Rather than address their complaints and improve the Music Store, <b>Andrew</b> has decided he will make himself rich exploiting students coding skills and then give up lecturing.
						</p>
						<p>
							<b>Andrew</b>'s plan is to have students create an online music store platform called BitterMusic.
						</p>
						<p>
							<b>Andrew</b> believes the absence of any similar existing online music store platform means BitterMusic will become very popular and he will become rich.
						</p>
						<p>
							<h4 style="color:#0099FF">
								<u>Story</u>
							</h4>
						</p>
						<p>
							<b>BitterMusic - An online Music Store that was built with bitterness.</b>
						</p>
						<p>
							Part of Bitter Family of Web Apps.
						</p>
						<p>
							Made by Timothy Pringgondhani
						</p>
					</div>
		        </div>
		    </div>
	    </center>
	</div>
	
	<%@ include file="footer.html" %>

</body>
</html>