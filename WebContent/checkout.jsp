<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="assign1.*, java.util.*"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<%@ include file="style.html" %>
	
	<title>Checkout</title>
</head>
<body background="default_back.jpg">

<%
	MusicDB musicDatabase = new MusicDB();
	Cart theCart = new Cart();

	try{	
	  	musicDatabase = (MusicDB)session.getAttribute("MusicDB");
	  	theCart = (Cart)session.getAttribute("UserCart");
	  	
	  	if (musicDatabase == null || theCart == null){
	  		response.sendRedirect("./search");
	  		return;
  		}
	}
	catch(Exception e){
  		// Go to Welcome page when there is error. (Usually caused by session not defined)
		response.sendRedirect("./search");
  		return;
	}
%>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="container">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="./search">
	            	<span class="glyphicon glyphicon-music"></span> 
	            	BitterMusic
	            </a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav">
	                <li class="">
	                    <a href="./search">Search</a>
	                </li>
     		        <li class="">
	                    <a href="./search?act=about">About</a>
	                </li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right">
					<li class="navbar-right">
	            		<a href="./search?act=cart">Shopping Cart (<%= theCart.getTotalItemInCart() %>)</a>
	            	</li>
	            </ul>
	        </div>
	    </div>
	</nav>

	<div class="container">
		<center>
			<form action="./search" method="POST">
			    <div class="row">
			    	<div class="col-lg-12">
		
						<h2 style="color:White" class="page-header">
			        		Checkout
			        	</h2>
				    
					   	<% 	if (theCart.getSongCart().size() > 0){	%>
					   	
					    <div class="col-xs-12 col-md-12">
					    	<h3 style="color:White">
					    		Songs <a style="font-size:75%; color:#0099FF" class="expandCollapseSong" href="#">(Collapse)</a>
							</h3>
					    </div>
						<div id="songSection" name="songSection" class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="left">
								<div class="panel panel-default col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table class="table" style="color:DarkGray">
						       				<thead>
						       					<th>
						       						#
						       					</th>
							       				<th>
													Title
							       				</th>
							       				<th>
													Artist
							       				</th>
							       				<th>
													Album
							       				</th>
							       				<th>
													Genre
							       				</th>
							       				<th>
													Publisher
							       				</th>
							       				<th>
													Year
							       				</th>
							       				<th>
													Price
							       				</th>
						       				</thead>
						       				<tbody>
							       				<% 	for (int i = 0; i <  theCart.getSongCart().size();i++){	
							       						Song s = theCart.getSongCart().get(i);
							       				%>
							       				<tr>
							       					<th scope="row"><%= i + 1 %></th>
													<td>
														<a style="color:#0099FF" href="<%= "./search?id=" + s.getSongID() + "&type=song&act=details" %>"><%= (s.getTitle() != null) ? s.getTitle() : "N/A" %></a>												
													</td>
													<td>
														<a href="<%= "./search?search=" + s.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (s.getArtist() != null) ? s.getArtist() : "N/A" %></a>								
													</td>
													<%
													   String albumID = (s.getAlbumID() != null) ? s.getAlbumID() : "N/A"; 
													   Album a;
													   
													   String album = "N/A";
													   String genre = "N/A";
													   String publisher = "N/A";
													   String year = "N/A";
													   
													   if (!albumID.equalsIgnoreCase("n/a")){
														   a = musicDatabase.getAlbum(albumID);
														   album = (a != null) ? a.getTitle() : "N/A";
														   genre = (a != null) ? a.getGenre() : "N/A";
														   publisher = (a != null) ? a.getPublisher() : "N/A";
														   year = (a != null) ? a.getYear(): "N/A";
													   }
													%>
													
													<td>
														<a style="color:#0099FF" href="<%= "./search?id=" + albumID + "&type=album&act=details" %>"><%= album %></a>																									
													</td>
													<td>
														<%= genre %>												
													</td>
													<td>
														<%= publisher %>												
													</td>
													<td>
														<%= year %>												
													</td>
													<td>
														$<%= (s.getPrice() != null) ? String.format("%.2f", Double.parseDouble(s.getPrice())) : "N/A" %>
													</td>
												</tr>
												<% 	}	%>
											</tbody>
						       			</table>
									</p>
								</div>
							</div>
						</div>
						
						<%	}	%>				
						<%	if (theCart.getAlbumCart().size() > 0){	%>
						
						<div class="col-xs-12 col-md-12">
					    	<h3 style="color:White">
					    		Albums <a style="font-size:75%; color:#0099FF" class="expandCollapseAlbum" href="#">(Collapse)</a>
							</h3>
					    </div>
						<div id="albumSection" name="albumSection" class="col-xs-12 col-md-12 img-rounded" style="background-color:White">									
							<div class="row" align="left">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table class="table" style="color:DarkGray">
						       				<thead>
						       					<th>
						       						#
						       					</th>
							       				<th>
													Title
							       				</th>
							       				<th>
													Artist
							       				</th>
							       				<th>
													Genre
							       				</th>
							       				<th>
													Publisher
							       				</th>
							       				<th>
													Year
							       				</th>
							       				<th>
													Price
							       				</th>
						       				</thead>
						       				<tbody>
							       				<% 	for (int i = 0; i <  theCart.getAlbumCart().size();i++){	
							       						Album a = theCart.getAlbumCart().get(i);
							       				%>
							       				<tr>
							       					<th scope="row"><%= i + 1 %></th>
													<td>
														<a style="color:#0099FF" href="<%= "./search?id=" + a.getID() + "&type=album&act=details" %>"><%= (a.getTitle() != null) ? a.getTitle() : "N/A" %></a>																									
													</td>
													<td>
														<a href="<%= "./search?search=" + a.getArtist() + "&advanced=on&search_option=artist&act=search" %>"><%= (a.getArtist() != null) ? a.getArtist() : "N/A" %></a>												
													</td>													
													<td>
														<%= (a.getGenre() != null) ? a.getGenre() : "N/A" %>														
													</td>
													<td>
														<%= (a.getPublisher() != null) ? a.getPublisher() : "N/A" %>														
													</td>
													<td>
														<%= (a.getYear() != null) ? a.getYear() : "N/A" %>														
													</td>
													<td>
														$<%= (a.getPrice() != null) ? String.format("%.2f", Double.parseDouble(a.getPrice())) : "N/A" %>																									
													</td>
												</tr>
												<% 	}	%>
											</tbody>
						       			</table>
									</p>
								</div>
							</div>
						</div>
					    
					    <%	}	%>
					    
					    <div class="col-xs-12 col-md-12">
					    	<br>
					    </div>
					    
					     <div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="right">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table>
							       			<tr>
							       				<td width="50%">
							       					Total Price
							       				</td>
							       				<td width="50%">
							       					<p style="text-align:right"><h5 style="color:#0099FF;">$<%= String.format("%.2f", theCart.getTotalPriceInCart()) %></h5></p>
							       				</td>
							       			</tr>
							       		</table>
									</p>
								</div>
							</div>
						</div>
					    
					    <div class="col-xs-12 col-md-12">
					    	<br>
					    </div>
					    
					    <div class="col-xs-12 col-md-12 img-rounded" style="background-color:White">					
							<div class="row" align="center">
								<div class="col-xs-12 col-md-12 img-rounded" style="background-color:White" >
						       		<p>
						       			<table>
						       				<tr>
						       					<h4 style="color:#0099FF">Do you want to continue checkout?</h4>
						       				</tr>
						       				<tr>
						       					<td>
						       						<button type="submit" class="btn btn-default" name="act" id="act" value="checkout_no">No</button>
						       					</td>
						       					<td>
						       						<button type="submit" class="btn btn-default" name="act" id="act" value="checkout_yes">Yes</button>
						       					</td>
						       				</tr>
						       			</table>
									</p>
								</div>
							</div>
						</div>
					</div>		    
				</div>				
			</form>
		</center>
	</div>
	
	<%@ include file="footer.html" %>
	
	<script type="text/javascript">	
		$(document).ready(function () {
	        $(".expandCollapseSong").click(function () {
	        	var value = $(this).text().toLowerCase();
	        	
	        	if (value == "(collapse)"){
	        		$('.expandCollapseSong')[0].innerHTML = "(Expand)";
	            	$("#songSection").hide("slow");
	        	}
	        	else{
	        		$('.expandCollapseSong')[0].innerHTML = "(Collapse)";
	            	$("#songSection").show("slow");
	        	}
	        });
	        
	        $(".expandCollapseAlbum").click(function () {
	        	var value = $(this).text().toLowerCase();
	        	
	        	if (value == "(collapse)"){
	        		$('.expandCollapseAlbum')[0].innerHTML = "(Expand)";
	            	$("#albumSection").hide("slow");
	        	}
	        	else{
	        		$('.expandCollapseAlbum')[0].innerHTML = "(Collapse)";
	            	$("#albumSection").show("slow");
	        	}
	        });
	    });
	</script>
	
</body>
</html>