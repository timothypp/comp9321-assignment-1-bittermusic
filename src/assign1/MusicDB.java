package assign1;
import java.util.*;

public class MusicDB {
	private ArrayList<Album> albumList;
	private ArrayList<Song> songList;

	public MusicDB() {
		albumList = new ArrayList<Album>();
		songList = new ArrayList<Song>();
	}

	public ArrayList<Album> getAlbumList() {
		return albumList;
	}
	
	public ArrayList<Song> getSongList() {
		return songList;
	}

	public void addAlbum(Album a) {
		this.albumList.add(a);
	}

	public void removeAlbum(String aID) {
		for (Album a : this.albumList) {
			if (a.getID().equals(aID)) {
				this.albumList.remove(a);
				break;
			}
		}
	}
	
	public Album getAlbum(String aID){
		for (Album a : this.albumList) {
			if (a.getID().equals(aID)) {
				return a;
			}
		}
		
		return null;
	}
	
	public void addSong(Song s) {
		this.songList.add(s);
	}

	public void removeSong(String sID) {
		for (Song s : this.songList) {
			if (s.getSongID().equals(sID)) {
				this.songList.remove(s);
				break;
			}
		}
	}
	
	public Song getSong(String sID){
		for (Song s : this.songList) {
			if (s.getSongID().equals(sID)) {
				return s;
			}
		}
		
		return null;
	}
}