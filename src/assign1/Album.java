package assign1;

public class Album {
	String artist;
	String title;
	String ID;
	String genre;
	String publisher;
	String year;
	String price;

	public Album() {

	}

	public Album(String a, String t, String i, String g, String p, String y, String pr) {
		super();

		this.artist = a;
		this.title = t;
		this.ID = i;
		this.genre = g;
		this.publisher = p;
		this.year = y;
		this.price = pr;
	}

	public String getArtist() {
		return this.artist;
	}

	public String getTitle() {
		return this.title;
	}

	public String getID() {
		return this.ID;
	}

	public String getGenre() {
		return this.genre;
	}

	public String getPublisher() {
		return this.publisher;
	}

	public String getYear() {
		return this.year;
	}

	public String getPrice() {
		return this.price;
	}

	public void setArtist(String a) {
		this.artist = a;
	}

	public void setTitle(String t) {
		this.title = t;
	}

	public void setID(String i) {
		this.ID = i;
	}

	public void setGenre(String g) {
		this.genre = g;
	}

	public void setPublisher(String p) {
		this.publisher = p;
	}

	public void setYear(String y) {
		this.year = y;
	}

	public void setPrice(String pr) {
		this.price = pr;
	}
}
