package assign1;

public class Song {
	String artist;
	String title;
	String albumID;
	String price;
	String songID;

	public Song() {

	}

	public Song(String a, String t, String aID, String p, String sID) {
		this.artist = a;
		this.title = t;
		this.albumID = aID;
		this.price = p;
		this.songID = sID;
	}

	public String getArtist() {
		return this.artist;
	}

	public String getTitle() {
		return this.title;
	}

	public String getAlbumID() {
		return this.albumID;
	}

	public String getPrice() {
		return this.price;
	}

	public String getSongID() {
		return this.songID;
	}

	public void setArtist(String a) {
		this.artist = a;
	}

	public void setTitle(String t) {
		this.title = t;
	}

	public void setAlbumID(String aID) {
		this.albumID = aID;
	}

	public void setPrice(String p) {
		this.price = p;
	}

	public void setSongID(String sID) {
		this.songID = sID;
	}
}
