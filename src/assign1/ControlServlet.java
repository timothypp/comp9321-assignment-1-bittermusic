package assign1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * Servlet implementation class ControlServlet
 */
@WebServlet(urlPatterns = "/search", displayName = "ControlServlet")
public class ControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private String search_value = "";
	private String search_advance = "";
	private String search_option = "";
			
	private String detail_id = "";
	private String detail_type = "";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlServlet() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = ((request.getParameter("act") == null) ? "unknown" : request.getParameter("act").toLowerCase());	
		String page_to = "";
		
		HttpSession session = request.getSession(true);
		createMusicDB(session);
		createCartSession(session);
		
		MusicDB musicDatabase = (MusicDB)session.getAttribute("MusicDB");
		
		
		if (action.equalsIgnoreCase("about")){
			page_to = "/about.jsp";
		}
		else if (action.equalsIgnoreCase("cart")){
			page_to = "/cart.jsp";
		}
		else if (action.equalsIgnoreCase("search")){
			page_to = "/result.jsp";
			searchData(request, response, musicDatabase);
		}
		else if (action.equalsIgnoreCase("details")){
			String type = ((request.getParameter("type") == null) ? "unknown" : request.getParameter("type").toLowerCase());	
			String detail_id = ((request.getParameter("id") == null) ? "unknown" : request.getParameter("id").toLowerCase());	
			
			this.detail_id = detail_id;
			detail_type = type;
			
			boolean result = handleDetail(request, musicDatabase, type, detail_id);

			
			if (result){
				page_to = "/details.jsp";
			}
			else{
				page_to = "/search.jsp";
			}
		}
		else{ 
			//Either Web App just started or Page Not Found
			page_to = "/search.jsp";

			ArrayList<Song> randomSong = getRandomSong(musicDatabase);			
			request.setAttribute("RandomSong", randomSong);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(page_to);
		rd.forward(request, response);
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = ((request.getParameter("act") == null) ? "unknown" : request.getParameter("act").toLowerCase());	
		String page_to = "";
		
		HttpSession session = request.getSession(true);
		createMusicDB(session);
		createCartSession(session);
		
		MusicDB musicDatabase = (MusicDB)session.getAttribute("MusicDB");
		Cart theCart = (Cart)session.getAttribute("UserCart");
		
		
		if (action.equalsIgnoreCase("add_to_cart") || action.equalsIgnoreCase("add_to_cart_detail")){
			
			String message = checkCartContent(session, request, musicDatabase, theCart);
			session.setAttribute("ErrorMessage", message);

			if (message.length() == 0){
				page_to = "/cart.jsp";
				addNewItemToCart(request, musicDatabase, theCart);
			}
			else{
				if (action.equalsIgnoreCase("add_to_cart")){
					page_to = "./search?search=" + search_value + "&advanced=" + search_advance + "&search_option=" + search_option
					+ "&act=search";	
					response.sendRedirect(page_to);
				}
				else if ( action.equalsIgnoreCase("add_to_cart_detail")){
					page_to = "./search?id=" + detail_id + "&type=" + detail_type + "&act=details";
					response.sendRedirect(page_to);
				}
				
				return;
			}
		}
		else if (action.equalsIgnoreCase("remove_from_cart")){
			page_to = "/cart.jsp";
			removeNewItemToCart(request, musicDatabase, theCart);
		}
		else if (action.equalsIgnoreCase("checkout_cart")){
			if (theCart.getTotalItemInCart() > 0){
				page_to = "/checkout.jsp";
			}
		}
		else if (action.equalsIgnoreCase("checkout_no") || action.equalsIgnoreCase("checkout_yes")){
			//User Select Yes or No on checkout
			invalidateSession(request, musicDatabase, theCart);
			
			if (action.equalsIgnoreCase("checkout_no")){
				request.setAttribute("DisplayMessage", "Thank you for shopping");
			}
			else{
				request.setAttribute("DisplayMessage", "Thank you for purchasing");
			}
			
			page_to = "/finish.jsp";
		}
		else{ 
			//Either Web App just started or Page Not Found or page with GET method run with POST method
			page_to = "/search.jsp";
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(page_to);
		rd.forward(request, response);
	}
	
	
	private void createCartSession(HttpSession session) {
		Cart theCart;

		if (session.getAttribute("UserCart") == null) {
			theCart = new Cart();
			session.setAttribute("UserCart", theCart);
			
			//System.out.println("UserCart Session created!");
		} 
		else {
			theCart = (Cart) session.getAttribute("UserCart");
			//System.out.println("UserCart Session already been created!");
		}
	}

	
	private void createMusicDB(HttpSession session) {
		MusicDB musicDatabase;

		if (session.getAttribute("MusicDB") == null) {
			musicDatabase = parseMusicDB(); // Create DB
			session.setAttribute("MusicDB", musicDatabase);
			
			//System.out.println("MusicDB Session created!");
		} 
		else {
			musicDatabase = (MusicDB) session.getAttribute("MusicDB");
			//System.out.println("MusicDB Session already been created!");
		}
	}

	
	private MusicDB parseMusicDB() {
		ServletContext context = getServletContext();
		InputSource xmlFile = new InputSource(context.getResourceAsStream("/WEB-INF/musicDb.xml"));

		MusicDB musicDatabase = new MusicDB();

		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			Document doc = builder.parse(xmlFile);
			
			XMLParser parser = new XMLParser();
			
			musicDatabase = parser.parseMusicDB_Album(musicDatabase, doc);
			musicDatabase = parser.parseMusicDB_Song(musicDatabase, doc);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}

		return musicDatabase;
	}
	
	
	private ArrayList<Song> getRandomSong(MusicDB musicDatabase){	
		ArrayList<Song> randomSong = new ArrayList<Song>();
		ArrayList<Integer> randomNumber = new ArrayList<Integer>();
		
	  	int maxRandomShown = 10;
	  	
	  	ArrayList<Song> songList = musicDatabase.getSongList();

		if (songList.size() < maxRandomShown){
			maxRandomShown = songList.size();
		}
		
		Random rn = new Random();
		
	    while (randomNumber.size() < maxRandomShown) {
	    	randomNumber.add(rn.nextInt(songList.size()) + 1);
	    }
	    
	    for (int i = 0; i < randomNumber.size(); i++){
	    	int number = randomNumber.get(i) - 1;
	    	randomSong.add(songList.get(number));
	    }
		
		return randomSong;
	}
	
	
	private void searchData(HttpServletRequest request, HttpServletResponse response, MusicDB musicDatabase){
		String search = ((request.getParameter("search") == null) ? "" : request.getParameter("search").toLowerCase());
		String advanceSearch = ((request.getParameter("advanced") == null) ? "no" : request.getParameter("advanced").toLowerCase());
		String searchOption = ((request.getParameter("search_option") == null) ? "anything" : request.getParameter("search_option").toLowerCase());
			
		search_value = search;
		search_advance = advanceSearch;
		search_option = searchOption;
		
		if (search.length() == 0 || musicDatabase == null){
	  		// Go to Welcome page when there is error. (Usually caused by session not defined)
		  	String site = new String("./search");
		  	response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
		    response.setHeader("Location", site); 
		    
		    return;
		}
		else{
			ArrayList<Album> albumResult = new ArrayList<Album>();
			ArrayList<Song> songsResult = new ArrayList<Song>();
	
			
			if (advanceSearch.equalsIgnoreCase("on") && !searchOption.equalsIgnoreCase("anything")){
				if (searchOption.equalsIgnoreCase("album")){
					for (Album a : musicDatabase.getAlbumList()){
						
						String artist = a.getArtist().toLowerCase();
						String title = a.getTitle().toLowerCase();
						String genre = a.getGenre().toLowerCase();
						String publisher = a.getPublisher().toLowerCase();
						String year = a.getYear().toLowerCase();
						
						if (artist.contains(search)){
							albumResult.add(a);
							continue;
						}
						if (title.contains(search)){
							albumResult.add(a);
							continue;
						}
						if (genre.contains(search)){
							albumResult.add(a);
							continue;
						}
						if (publisher.contains(search)){
							albumResult.add(a);
							continue;
						}
						if (year.contains(search)){
							albumResult.add(a);
							continue;
						}
						
					}
				}
				else if (searchOption.equalsIgnoreCase("artist")){
					for (Album a : musicDatabase.getAlbumList()){
						
						String artist = a.getArtist().toLowerCase();
						if (artist.contains(search)){
							albumResult.add(a);
						}
						
					}
					
					for (Song s : musicDatabase.getSongList()){
						
						String artist = s.getArtist().toLowerCase();
						if (artist.contains(search)){
							songsResult.add(s);
						}
						
					}
				}
				else if (searchOption.equalsIgnoreCase("songs")){
					for (Song s : musicDatabase.getSongList()){
						
						String artist = s.getArtist().toLowerCase();
						String title = s.getTitle().toLowerCase();
						
						if (artist.contains(search)){
							songsResult.add(s);
							continue;
						}
						if (title.contains(search)){
							songsResult.add(s);
							continue;
						}
						
					}
				}
			}
			else{
				for (Album a : musicDatabase.getAlbumList()){
					
					String artist = a.getArtist().toLowerCase();
					String title = a.getTitle().toLowerCase();
					String genre = a.getGenre().toLowerCase();
					String publisher = a.getPublisher().toLowerCase();
					String year = a.getYear().toLowerCase();
					
					if (artist.contains(search)){
						albumResult.add(a);
						continue;
					}
					if (title.contains(search)){
						albumResult.add(a);
						continue;
					}
					if (genre.contains(search)){
						albumResult.add(a);
						continue;
					}
					if (publisher.contains(search)){
						albumResult.add(a);
						continue;
					}
					if (year.contains(search)){
						albumResult.add(a);
						continue;
					}
				}
				
				for (Song s : musicDatabase.getSongList()){
					
					String artist = s.getArtist().toLowerCase();
					String title = s.getTitle().toLowerCase();
					
					if (artist.contains(search)){
						songsResult.add(s);
						continue;
					}
					if (title.contains(search)){
						songsResult.add(s);
						continue;
					}
					
				}
			}
			
			request.setAttribute("SearchResultSong", songsResult);
			request.setAttribute("SearchResultAlbum", albumResult);
		}		
	}
	
	
	private void addNewItemToCart(HttpServletRequest request, MusicDB musicDatabase, Cart theCart){		
		String[] list_of_song_id = ((request.getParameterValues("sSet") == null) ? new String[0] : request.getParameterValues("sSet"));	    
		String[] list_of_album_id = ((request.getParameterValues("aSet") == null) ? new String[0] : request.getParameterValues("aSet"));	    

		//Add to Cart
		
		for (String album_id : list_of_album_id){
			Album theAlbum = musicDatabase.getAlbum(album_id);
			
			boolean found = false;

			if (theCart.getAlbumCart().contains(theAlbum)){
				found = true;
			}
			
			if (!found){
				theCart.addAlbum(theAlbum);
			}
			//System.out.println(album_id);
			//System.out.println(theAlbum.getTitle());
		}
		
		for (String song_id : list_of_song_id){
			Song theSong = musicDatabase.getSong(song_id);
			
			// If album is already added, then songs where its album is in cart will not be added
			boolean found = false;
			for (Album a : theCart.getAlbumCart()){
				if (a.getID().equalsIgnoreCase(theSong.getAlbumID())){
					found = true;
					break;
				}
			}
			
			if (theCart.getSongCart().contains(theSong)){
				found = true;
			}
			
			if (!found){
				theCart.addSong(theSong);
			}
			
			//System.out.println(song_id);
			//System.out.println(theSong.getTitle());
		}
		
		HttpSession session = request.getSession(true);
		session.setAttribute("UserCart", theCart);
	}
	
	
	private void removeNewItemToCart(HttpServletRequest request, MusicDB musicDatabase, Cart theCart){
		String[] list_of_song_id = ((request.getParameterValues("sSet") == null) ? new String[0] : request.getParameterValues("sSet"));	    
		String[] list_of_album_id = ((request.getParameterValues("aSet") == null) ? new String[0] : request.getParameterValues("aSet"));	    
		
		//Remove from Cart
		
		for (String song_id : list_of_song_id){
			theCart.removeSong(song_id);
			
			//System.out.println(song_id);
		}
		
		for (String album_id : list_of_album_id){
			theCart.removeAlbum(album_id);
			
			//System.out.println(album_id);
		}
		
		HttpSession session = request.getSession(true);
		session.setAttribute("UserCart", theCart);
	}
	
	
	private String checkCartContent(HttpSession session, HttpServletRequest request, MusicDB musicDatabase, Cart theCart){
		String[] list_of_song_id = ((request.getParameterValues("sSet") == null) ? new String[0] : request.getParameterValues("sSet"));	    
		String[] list_of_album_id = ((request.getParameterValues("aSet") == null) ? new String[0] : request.getParameterValues("aSet"));	    
		
		String message_error = "";
		
		long error_album = 0;
		
		ArrayList<String> cart_song = new ArrayList<String>(Arrays.asList(list_of_song_id));
		ArrayList<String> cart_album = new ArrayList<String>(Arrays.asList(list_of_album_id));
		ArrayList<String> cart_album_song = new ArrayList<String>();

		for (Album a : theCart.getAlbumCart()){
			for (Song s : musicDatabase.getSongList()){
				if (s.getAlbumID().equalsIgnoreCase(a.getID())){
					cart_album_song.add(s.getSongID());
				}
			}
		}
		
		if (list_of_song_id.length > 0){
			for (Song s : theCart.getSongCart()){
		    	String song_id = s.getSongID();
		    	
		    	if (cart_song.contains(song_id)){
		    		message_error += "The Song : \"" + s.getTitle() + "\" is already in Shopping Cart!<br><br>";
		    	}
			}
			
			for (String sID : list_of_song_id){
				Song s = musicDatabase.getSong(sID);
				
				if (cart_album_song.contains(sID)){
		    		message_error += "The Song : \"" + s.getTitle() + "\" is already a part of an Album in Shopping Cart!<br><br>";
		    	}
			}
		}
		
		if (list_of_album_id.length > 0){
			for (Album a : theCart.getAlbumCart()){		    	
		    	if (cart_album.contains(a.getID())){
		    		message_error += "The Album : \"" + a.getTitle() + "\" is already in Shopping Cart!<br><br>";
		    	}
			}
			
			for (String aID : list_of_album_id){
				Album a = musicDatabase.getAlbum(aID);
				
				for (Song s : theCart.getSongCart()){		
	    			if (s.getAlbumID().equalsIgnoreCase(a.getID())){
	    				error_album++;
	    				message_error += "The Album : \"" + a.getTitle() + "\" has the Song : \"" + s.getTitle() + "\" that is already in the Shopping Cart! *<br><br>";
	    			}
	    		}
			}
		}

		if (error_album > 0){
			message_error += "* To Fix this problem, please remove the Song causing duplicate in the shopping cart!";
		}
		
		return message_error;
	}
	
	
	private void invalidateSession(HttpServletRequest request, MusicDB musicDatabase, Cart theCart){
		
		theCart.clearCart(); //Clear Cart Content (No Need to Invalidate Session cause data is stored in this object)
		
		HttpSession session = request.getSession(true);
		session.setAttribute("UserCart", theCart);
		
		search_value = "";
		search_advance = "";
		search_option = "";
				
		detail_id = "";
		detail_type = "";
		
	}
	
	
	private boolean handleDetail(HttpServletRequest request, MusicDB musicDatabase, String type, String detail_id){
		boolean found = false;
		
		if (type.equalsIgnoreCase("song")){
			Song s = musicDatabase.getSong(detail_id);
			
			if (s != null){
				found = true;
				request.setAttribute("Type", "Song");
				request.setAttribute("Data", s);
			}
		}
		else if (type.equalsIgnoreCase("album")){
			Album a = musicDatabase.getAlbum(detail_id);
			
			if (a != null){
				found = true;
				request.setAttribute("Type", "Album");
				request.setAttribute("Data", a);
			}
		}
	
		return found;
	}
}
