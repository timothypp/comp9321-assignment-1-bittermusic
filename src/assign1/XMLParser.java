package assign1;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLParser {

	public XMLParser(){
		
	}
	
	protected MusicDB parseMusicDB_Album(MusicDB musicDatabase, Document doc){
		NodeList allAlbums = doc.getElementsByTagName("albumList");
		
		for (int i = 0; i < allAlbums.getLength(); i++) {
			Album a = new Album();

			Node album = allAlbums.item(i);
			NodeList albumElements = album.getChildNodes();

			for (int j = 0; j < albumElements.getLength(); j++) {
				Node e = albumElements.item(j);

				if (e.getNodeName().equalsIgnoreCase("artist"))
					a.setArtist(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("title"))
					a.setTitle(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("id"))
					a.setID(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("genre"))
					a.setGenre(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("publisher"))
					a.setPublisher(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("year"))
					a.setYear(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("price"))
					a.setPrice(e.getTextContent());
			}

			musicDatabase.addAlbum(a);
		}

		//System.out.println("Album count: " + musicDatabase.getAlbumList().size());
		
		return musicDatabase;
	}

	protected MusicDB parseMusicDB_Song(MusicDB musicDatabase, Document doc) {
		NodeList allSongs = doc.getElementsByTagName("songList");
		
		for (int i = 0; i < allSongs.getLength(); i++) {
			Song s = new Song();
			
			Node song = allSongs.item(i);
			NodeList songElements = song.getChildNodes();
			
			for (int j = 0; j < songElements.getLength(); j++) {
				Node e = songElements.item(j);

				if (e.getNodeName().equalsIgnoreCase("artist"))
					s.setArtist(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("title"))
					s.setTitle(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("albumid"))
					s.setAlbumID(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("price"))
					s.setPrice(e.getTextContent());
				if (e.getNodeName().equalsIgnoreCase("songid"))
					s.setSongID(e.getTextContent());
			}
			
			musicDatabase.addSong(s);
		}
		
		//System.out.println("Song count: " + musicDatabase.getSongList().size());
		
		return musicDatabase;
	}
}
