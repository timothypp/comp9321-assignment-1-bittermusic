package assign1;
import java.util.*;

public class Cart {
	
	ArrayList<Song> songCart;
	ArrayList<Album> albumCart;
	
	public Cart() {
		songCart = new ArrayList<Song>();
		albumCart = new ArrayList<Album>();
	}
	
	public ArrayList<Song> getSongCart(){
		return songCart;
	}
	
	public ArrayList<Album> getAlbumCart(){
		return albumCart;
	}

	public void addSong(Song s) {
		songCart.add(s);
	}

	public void removeSong(String sID) {
		for (Song s : this.songCart) {
			if (s.getSongID().equals(sID)) {
				this.songCart.remove(s);
				break;
			}
		}
	}
	
	public void addAlbum(Album a) {
		albumCart.add(a);
	}

	public void removeAlbum(String aID) {
		for (Album a : this.albumCart) {
			if (a.getID().equals(aID)) {
				this.albumCart.remove(a);
				break;
			}
		}
	}
	
	public long getTotalItemInCart(){
		return albumCart.size() + songCart.size();
	}
	
	public void clearCart(){
		//Remove all content inside a cart
		
		this.albumCart.clear();
		this.songCart.clear();
	}
	
	public double getTotalPriceInCart(){
		double price = 0.0;
		
		for (Album a : this.albumCart) {
			price += Double.parseDouble(a.getPrice());
		}
		
		for (Song s : this.songCart) {
			price += Double.parseDouble(s.getPrice());
		}
		
		return price;
	}
}
